const Sequelize = require('sequelize');

const directorModel = require('./models/director');
const genreModel = require('./models/genre');
const movieModel = require('./models/movie');
const actorModel = require('./models/actor');
const movieActorModel = require('./models/movieActor');
const bookingModel = require('./models/booking');
const copyModel = require('./models/copy');
const memberModel = require('./models/member');

//1 db name 2 user 3 password 4 Obj conf

const sequelize = new Sequelize('video-club', 'root', 'abcd1234', {
    host: 'localhost',//Direccion de nuestros RDBMs
    dialect: 'mysql'
});

const Director = directorModel(sequelize, Sequelize);
const Genre = genreModel(sequelize, Sequelize);
const Movie = movieModel(sequelize, Sequelize);
const Actor = actorModel(sequelize, Sequelize);
const MovieActor = movieActorModel(sequelize, Sequelize);
const Booking = bookingModel(sequelize, Sequelize);
const Copy = copyModel(sequelize, Sequelize);
const Member = memberModel(sequelize, Sequelize);

//Un genero puede tener muchas peliculas
Genre.hasMany(Movie, {as: 'movies'});

//Una pelicula puede tener un genero
Movie.belongsTo(Genre, {as:'genre'});

//Un director puede tener muchas peliculas
Director.hasMany(Movie, {as: 'movies'});

//Una pelicula puede tener un director
Movie.belongsTo(Director, {as:'director'});

//Un actor participa en muchas peliculas
MovieActor.belongsTo(Movie, {foreignKey: 'movieId'});

//En una pelicula participan muchos actores
MovieActor.belongsTo(Actor, {foreignKey: 'actorId'});

Movie.belongsToMany(Actor, {
    foreignKey: 'actorId',
    as: 'actors',
    through: 'moviesActors'
});

Actor.belongsToMany(Movie, {
    foreignKey: 'movieId',
    as: 'movies',
    through: 'moviesActors'
})

//Una pelicula tiene muchas copias
Movie.hasMany(Copy, {as: 'copies'});

//Una copia solo puede ser de una pelicula
Copy.belongsTo(Movie, {as:'movies'});

//Una copia tiene muchas reservaciones
Copy.hasMany(Booking, {as: 'bookings'});

//En una reservacion hay una copias
Booking.belongsTo(Copy, {as:'copies'});

//Un miembro tiene muchas reservaciones
Member.hasMany(Booking, {as: 'bookings'});

//Una reservacion tiene un miembro
Booking.belongsTo(Member, {as:'members'});

sequelize.sync({
    force: true
}).then(()=>{
    console.log("Base de datos actualizada correctamente");
});

module.exports = {Director, Genre, Movie, Actor, MovieActor, Copy, Booking, Member};