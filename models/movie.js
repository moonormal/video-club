module.exports = (sequelize, type) => {
    const Movie = sequelize.define('movies', {
        id : {type : type.INTEGER, primaryKey: true, autoIncrement: true},
        tittle: {type: type.STRING, notNull: true}
    });
    return Movie;
};