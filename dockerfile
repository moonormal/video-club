FROM node
MAINTAINER Melissa Garcia
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 3000
CMD npm start