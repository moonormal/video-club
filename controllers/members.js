const express = require('express');
const { Member } = require('../db');

//RESFULL = GET, POST, PUT, PACH Y DELETE
//Modelo = Una estructura de datos que representa una entidad real
function list(req, res, next){
    Member.findAll()
        .then(objects =>res.json(objects))
        .catch(err => res.send(err));
}
 
function index(req, res, next){
    const id= req.params.id;
    Member.findByPk(id)
        .then(object => res.json(object))
        .catch(err => res.send(err));
}

function create(req, res, next){
    const name= req.body.name;
    const lastName= req.body.lastName;
    const address= req.body.address;
    const phone= req.body.phone;
    const status= req.body.status;

    let member = new Object({
        name:name,
        lastName:lastName,
        address:address,
        phone:phone,
        status:status
    });

    Member.create(member)
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
    
}

function replace(req, res, next){
/*
    const id= req.params.id;
    Member.findByPk(id)
        .then((object) => {
            object.update({name: req.body.name, lastName: req.body.lastName, address= req.body.address, phone= req.body.phone, status= req.body.status})
                .then(member => res.json(member));
        })
        .catch(err => res.send(err));
*/
}

function edit(req, res, next){
/*
    const id= req.params.id;
    Member.findByPk(id)
        .then((object) => {
            const name = req.body.name ? req.body.name : object.name;
            const lastName = req.body.lastName ? req.body.lastName : object.lastName;
            object.update({name:name, lastName:lastName})
                .then(member => res.json(member));
        })
        .catch(err => res.send(err));
*/
}

function destroy(req, res, next){
    const id = req.params.id;
    Member.destroy({where:{id:id}})
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
}

module.exports = {
    list, index, create, replace, edit, destroy
}