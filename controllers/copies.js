const express = require('express');
const { Copy } = require('../db');

//RESFULL = GET, POST, PUT, PACH Y DELETE
//Modelo = Una estructura de datos que representa una entidad real
function list(req, res, next){
    Copy.findAll()
        .then(objects =>res.json(objects))
        .catch(err => res.send(err));
}
 
function index(req, res, next){
    const id= req.params.id;
    Copy.findByPk(id)
        .then(object => res.json(object))
        .catch(err => res.send(err));
}

function create(req, res, next){
    const number= req.body.number;
    const format= req.body.format;
    const movieId = req.body.movieId;
    const status = req.body.status;

    let copy = new Object({
        number:number,
        format:format,
        movieId:movieId,
        status:status
    });

    Copy.create(copy)
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
    
}

function replace(req, res, next){
/*
    const id= req.params.id;
    Copy.findByPk(id)
        .then((object) => {
            object.update({name: req.body.name, lastName: req.body.lastName})
                .then(copy => res.json(copy));
        })
        .catch(err => res.send(err));
*/
}

function edit(req, res, next){
/*
    const id= req.params.id;
    Copy.findByPk(id)
        .then((object) => {
            const name = req.body.name ? req.body.name : object.name;
            const lastName = req.body.lastName ? req.body.lastName : object.lastName;
            object.update({name:name, lastName:lastName})
                .then(copy => res.json(copy));
        })
        .catch(err => res.send(err));
*/
}

function destroy(req, res, next){
    const id = req.params.id;
    Copy.destroy({where:{id:id}})
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
}

module.exports = {
    list, index, create, replace, edit, destroy
}