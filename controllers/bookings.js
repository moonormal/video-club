const express = require('express');
const { Booking } = require('../db');

//RESFULL = GET, POST, PUT, PACH Y DELETE
//Modelo = Una estructura de datos que representa una entidad real
function list(req, res, next){
    Booking.findAll({include:['copies','members']})
        .then(objects =>res.json(objects))
        .catch(err => res.send(err));
}
 
function index(req, res, next){
    const id= req.params.id;
    Booking.findByPk(id)
        .then(object => res.json(object))
        .catch(err => res.send(err));
}

function create(req, res, next){
    const date= req.body.date;
    const memberId= req.body.memberId;
    const copyId= req.body.copyId;

    let booking = new Object({
        date:date,
        memberId:memberId,
        copyId:copyId
    });

    Booking.create(booking)
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
    
}

function replace(req, res, next){
/*
    const id= req.params.id;
    Booking.findByPk(id)
        .then((object) => {
            object.update({date: req.body.date})
                .then(booking => res.json(booking));
        })
        .catch(err => res.send(err));
*/
}

function edit(req, res, next){
/*
    const id= req.params.id;
    Booking.findByPk(id)
        .then((object) => {
            const name = req.body.name ? req.body.name : object.name;
            const lastName = req.body.lastName ? req.body.lastName : object.lastName;
            object.update({name:name, lastName:lastName})
                .then(booking => res.json(booking));
        })
        .catch(err => res.send(err));
*/
}

function destroy(req, res, next){
    const id = req.params.id;
    Booking.destroy({where:{id:id}})
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
}

module.exports = {
    list, index, create, replace, edit, destroy
}