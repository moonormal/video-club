const express = require('express');
const { Movie } = require('../db');
const { Actor } = require('../db');

//RESFULL = GET, POST, PUT, PACH Y DELETE
//Modelo = Una estructura de datos que representa una entidad real
function list(req, res, next){
    Movie.findAll({include:['genre','director']})
        .then(objects =>res.json(objects))
        .catch(err => res.send(err));
}
 
function index(req, res, next){
    const id= req.params.id;
    Movie.findByPk(id)
        .then(object => res.json(object))
        .catch(err => res.send(err));
}

function addActor(req, res, next){
    const idMovie = req.body.idMovie;
    const idActor = req.body.idActor;

    Movie.findByPk(idMovie).then((movie) => {
        Actor.findByPk(idActor).then((actor)=>{
            movie.addActor(actor);
            res.json(movie)
        });
    });
}

function create(req, res, next){
    const tittle= req.body.tittle;
    const genreId= req.body.genreId;
    const directorId= req.body.directorId

    let movie = new Object({
        tittle:tittle,
        genreId:genreId,
        directorId:directorId
    });

    Movie.create(movie)
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
    
}

function replace(req, res, next){
/*
    const id= req.params.id;
    Movie.findByPk(id)
        .then((object) => {
            object.update({name: req.body.name, lastName: req.body.lastName})
                .then(movie => res.json(movie));
        })
        .catch(err => res.send(err));
*/
}

function edit(req, res, next){
/*
    const id= req.params.id;
    Movie.findByPk(id)
        .then((object) => {
            const name = req.body.name ? req.body.name : object.name;
            const lastName = req.body.lastName ? req.body.lastName : object.lastName;
            object.update({name:name, lastName:lastName})
                .then(movie => res.json(movie));
        })
        .catch(err => res.send(err));
*/
}

function destroy(req, res, next){
    const id = req.params.id;
    Movie.destroy({where:{id:id}})
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
}

module.exports = {
    list, index, create, replace, edit, destroy, addActor
}