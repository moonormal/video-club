/*const express = require('express');
const { Director } = require('../db');

//RESFULL = GET, POST, PUT, PACH Y DELETE
//Modelo = Una estructura de datos que representa una entidad real
function list(req, res, next){
    res.send('Lista de usuarios del sistema');
}
 
function index(req, res, next){
    res.send(`Usuario del sistema con un ID = ${req.params.id}`);
}

function create(req, res, next){
    const name= req.body.name;
    const lastName= req.body.lastName;

    let director = new Object({
        name:name,
        lastName:lastName
    });

    Director.create(director)
        .then(obj => res.json(obj))
        .catch(err => res.send(err));


}

function replace(req, res, next){
    res.send(`Remplazo un usuario con ID = ${req.params.id} por otro`);
}

function edit(req, res, next){
    res.send(`Remplazo las propiedades de un usuario con ID = ${req.params.id} por otro`);
}

function destroy(req, res, next){
    res.send(`Elimino un usuario con ID = ${req.params.id} por otro`);
}

module.exports = {
    list, index, create, replace, edit, destroy
}
*/
const express = require('express');
const { Director } = require('../db');

//RESFULL = GET, POST, PUT, PACH Y DELETE
//Modelo = Una estructura de datos que representa una entidad real
function list(req, res, next){
    Director.findAll({include:['movies']})
        .then(objects =>res.json(objects))
        .catch(err => res.send(err));
}
 
function index(req, res, next){
    const id= req.params.id;
    Director.findByPk(id)
        .then(objects =>res.json(objects))
        .catch(err => res.send(err));
}

function create(req, res, next){
    const name= req.body.name;
    const lastName= req.body.lastName;

    let director = new Object({
        name:name,
        lastName:lastName
    });

    Director.create(director)
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
    
}

function replace(req, res, next){
    const id= req.params.id;
    Director.findByPk(id)
        .then((object) => {
            object.update({name: req.body.name, lastName: req.body.lastName})
                .then(director => res.json(director));
        })
        .catch(err => res.send(err));
}

function edit(req, res, next){
    const id= req.params.id;
    Director.findByPk(id)
        .then((object) => {
            const name = req.body.name ? req.body.name : object.name;
            const lastName = req.body.lastName ? req.body.lastName : object.lastName;
            object.update({name:name, lastName:lastName})
                .then(director => res.json(director));
        })
        .catch(err => res.send(err));
}

function destroy(req, res, next){
    const id = req.params.id;
    Director.destroy({where:{id:id}})
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
}

module.exports = {
    list, index, create, replace, edit, destroy
}